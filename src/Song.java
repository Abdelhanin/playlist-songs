public class Song {
    public Song(String title, double duration) {
        this.title = title;
        this.duration = duration;
    }

    private String title;
    private double duration;


    public String getTitle() {
        return title;
    }

    @Override
    public String toString() {
        return "Song{" +
                "title='" + title + '\'' +
                ", duration=" + duration +
                '}';
    }
}
